export const danhSachSanPham = (value) => {
  let danhSach = "";
  value.forEach((item) => {
    danhSach += ` 
    <div class='col-3 '>
      <div class="card text-white bg-primary p-2">
      <img class="card-img-top" src="${item.img}" alt="">
      <div div class="card-body">
        <h4 class="card-title">${
          item.name.length > 13 ? item.name.substr(0, 13) + "..." : item.name
        }</h4>
        <button id='${
          item.id
        }' class='btn btn-success iphone'  >Add to cart</button>
      </div>
      </div>
     </div>
    `;
  });
  document.getElementById("danhSachSanPham").innerHTML = danhSach;
};

let cart = [];
export const soLuongIphone = (arrPhone) => {
  let arrButton = document.querySelectorAll(".iphone");
  arrButton.forEach((item) => {
    item.addEventListener("click", () => {
      arrPhone.forEach((phone) => {
        if (item.id == phone.id) {
          let index = cart.findIndex((sanPham) => sanPham.id === item.id);
          if (index === -1) {
            cart.push({ ...phone, amount: 1 });
          } else {
            gioiHanSoLuong();
            cart[index].amount += 1;
          }
        }
      });
      console.log(cart);
      renderGioHang();
    });
  });
};

const renderGioHang = () => {
  let cartList = "";
  cart.forEach((item) => {
    cartList += ` <tr>
                  <td><img class='w-100 p-2' src="${item.img}" alt=""></td>
                  <td>${item.name}</td>
                  <td>${item.price}</td>
                  <td class=' text-center gioHangIphone' id='${item.id}'>
                  <button onclick='giamSoLuong(${item.id},false)' class="btn btn-primary">
                  Giảm
                  </button><br />
                  ${item.amount}
                  <button onclick='tangSoLuong(${item.id},true)' class="btn btn-success">
                  Tăng
                  </button></td>
                  <td>
                  <button onclick='xoaIphone(${item.id})' class='btn btn-danger'>Xóa</button>
                  </td>
                  </tr>
                 `;
    clearGioHang(item.id);
  });
  document.getElementById("tblDanhSachIphone").innerHTML = cartList;
  tongTien();
};

export const giamSoLuong = (idItemGioHang, value) => {
  if (value == false) {
    cart[idItemGioHang - 1].amount -= 1;
  }
  renderGioHang();
};
export const tangSoLuong = (idItemGioHang, value) => {
  if (value == true) {
    cart[idItemGioHang - 1].amount += 1;
  }
  gioiHanSoLuong();
  renderGioHang();
};

// export const tangGiamSoLuong = (value) => {
//   let gioHangIphone = document.querySelectorAll(".gioHangIphone");
//   gioHangIphone.forEach((i) => {
//     var index = cart.findIndex((sanPham) => {
//       return sanPham.id === i.id;
//     });
//     console.log("🚀 ~ file: controller-v2.js:78 ~ index ~ index", index);
//     if (value == true) {
//       renderGioHang();
//       gioiHanSoLuong();
//       cart[index].amount += 1;
//     } else {
//       renderGioHang();
//       cart[index].amount -= 1;
//     }
//   });
// };

const gioiHanSoLuong = () => {
  if (cart.length > 0) {
    cart.forEach((i) => {
      if (i.amount > 10) {
        return $("#gioiHanSoLuong").modal("show");
      }
    });
  }
};

export const tongTien = () => {
  let soTien = 0;
  cart.forEach((i) => {
    soTien += i.amount * i.price;
  });
  document.getElementById("soTien").innerHTML = soTien;
};

export const clearGioHang = (idItemGioHang) => {
  console.log(
    "🚀 ~ file: controller-v2.js:126 ~ clearGioHang ~ idItemGioHang",
    idItemGioHang
  );
  let clear = document.getElementById("clear");
  clear.addEventListener("click", () => {
    console.log("hello");
  });
};
